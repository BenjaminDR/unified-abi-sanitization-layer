#define TLS_TRUSTED_SP              8       // Offset in the thread local storage (TLS) where the last 
                                            // trusted stack pointer is stored.

#define ECALL_STACK_OFFSET          -66224  // Location of the ECALL stack base, defined as an 
                                            // offset to the TCS.

#define ERESOLVE_STACK_OFFSET       -65536  // Location of the ERESOLVE stack base, defined as an
                                            // offset to the TCS.


#define API_ROOT_ECALL_FUNC         ecall_entry
#define API_NESTED_ECALL_FUNC       ecall_entry
#define API_ERESOLVE_FUNC           resolve_entry
#define SIMULATION_EXIT_FUNC        simulation_enclave_exit

#define SIMULATION_MODE             1
#define EXCEPTION_RESOLVEMENT       1
