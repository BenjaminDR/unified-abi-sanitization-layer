# Test Enclave

You can use this enclave to test the ABI in an enclave build with the Intel-sdk after following the steps in `../README.md`.

You should be able to put any Enclave source files here as long as you copy the extra arguments to the linker in `Makefile`.

## Extra linking options

TODO
