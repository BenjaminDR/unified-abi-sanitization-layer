#include "Enclave_u.h"
#include <errno.h>

typedef struct ms_test_ecall_t {
	int ms_retval;
	int ms_a;
} ms_test_ecall_t;

typedef struct ms_test_ocall_t {
	int ms_a;
} ms_test_ocall_t;

static sgx_status_t SGX_CDECL Enclave_test_ocall(void* pms)
{
	ms_test_ocall_t* ms = SGX_CAST(ms_test_ocall_t*, pms);
	test_ocall(ms->ms_a);

	return SGX_SUCCESS;
}

static const struct {
	size_t nr_ocall;
	void * table[1];
} ocall_table_Enclave = {
	1,
	{
		(void*)Enclave_test_ocall,
	}
};
sgx_status_t test_ecall(sgx_enclave_id_t eid, int* retval, int a)
{
	sgx_status_t status;
	ms_test_ecall_t ms;
	ms.ms_a = a;
	status = sgx_ecall(eid, 0, &ocall_table_Enclave, &ms);
	if (status == SGX_SUCCESS && retval) *retval = ms.ms_retval;
	return status;
}

