#include "sgx_urts.h"
#include "Enclave_u.h"
#include <iostream>

sgx_enclave_id_t global_eid = 0;


/* Initialize the enclave:
 *   Call sgx_create_enclave to initialize an enclave instance
 */
int initialize_enclave(void)
{
    sgx_status_t ret = SGX_ERROR_UNEXPECTED;

    /* Call sgx_create_enclave to initialize an enclave instance */
    /* Debug Support: set 2nd parameter to 1 */
    ret = sgx_create_enclave("enclave.signed.so", SGX_DEBUG_FLAG, NULL, NULL, &global_eid, NULL);
    if (ret != SGX_SUCCESS) {
        //print_error_message(ret);
        std::cout << "Error initializing enclave";
        return -1;
    }

    return 0;
}

// ------------------------------------------------------------------ //

int n_recursive_calls = 3;

void test_ocall(int a) {
    std::cout << "OCall received with a = " << a << std::endl;
    if (n_recursive_calls > 0) {
        n_recursive_calls--;
        std::cout << "Do recursive ECall with a = " << a - 1 << std::endl;
        int ptr;
        test_ecall(global_eid, &ptr, a - 1);
    }
    return;
}

int main(int argc, char const *argv[]) {
    if (argc != 2) {
        std::cout << "Usage: ./app <a>\n";
        return 0;
    }

    initialize_enclave();

    int ptr;
    int a = atoi(argv[1]);
    std::cout << "Do ECall with a = " << a << std::endl;
    sgx_status_t status = test_ecall(global_eid, &ptr, a);
    std::cout << ptr << std::endl;

    return 0;
}

