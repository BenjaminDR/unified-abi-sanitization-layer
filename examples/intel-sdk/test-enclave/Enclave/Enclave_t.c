#include "Enclave_t.h"

#include "sgx_trts.h" /* for sgx_ocalloc, sgx_is_outside_enclave */
#include "sgx_lfence.h" /* for sgx_lfence */

#include <errno.h>
#include <mbusafecrt.h> /* for memcpy_s etc */
#include <stdlib.h> /* for malloc/free etc */

#define CHECK_REF_POINTER(ptr, siz) do {	\
	if (!(ptr) || ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define CHECK_UNIQUE_POINTER(ptr, siz) do {	\
	if ((ptr) && ! sgx_is_outside_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define CHECK_ENCLAVE_POINTER(ptr, siz) do {	\
	if ((ptr) && ! sgx_is_within_enclave((ptr), (siz)))	\
		return SGX_ERROR_INVALID_PARAMETER;\
} while (0)

#define ADD_ASSIGN_OVERFLOW(a, b) (	\
	((a) += (b)) < (b)	\
)


typedef struct ms_test_ecall_t {
	int ms_retval;
	int ms_a;
} ms_test_ecall_t;

typedef struct ms_test_ocall_t {
	int ms_a;
} ms_test_ocall_t;

static sgx_status_t SGX_CDECL sgx_test_ecall(void* pms)
{
	CHECK_REF_POINTER(pms, sizeof(ms_test_ecall_t));
	//
	// fence after pointer checks
	//
	sgx_lfence();
	ms_test_ecall_t* ms = SGX_CAST(ms_test_ecall_t*, pms);
	sgx_status_t status = SGX_SUCCESS;



	ms->ms_retval = test_ecall(ms->ms_a);


	return status;
}

SGX_EXTERNC const struct {
	size_t nr_ecall;
	struct {void* ecall_addr; uint8_t is_priv; uint8_t is_switchless;} ecall_table[1];
} g_ecall_table = {
	1,
	{
		{(void*)(uintptr_t)sgx_test_ecall, 0, 0},
	}
};

SGX_EXTERNC const struct {
	size_t nr_ocall;
	uint8_t entry_table[1][1];
} g_dyn_entry_table = {
	1,
	{
		{0, },
	}
};


sgx_status_t SGX_CDECL test_ocall(int a)
{
	sgx_status_t status = SGX_SUCCESS;

	ms_test_ocall_t* ms = NULL;
	size_t ocalloc_size = sizeof(ms_test_ocall_t);
	void *__tmp = NULL;


	__tmp = sgx_ocalloc(ocalloc_size);
	if (__tmp == NULL) {
		sgx_ocfree();
		return SGX_ERROR_UNEXPECTED;
	}
	ms = (ms_test_ocall_t*)__tmp;
	__tmp = (void *)((size_t)__tmp + sizeof(ms_test_ocall_t));
	ocalloc_size -= sizeof(ms_test_ocall_t);

	ms->ms_a = a;
	status = sgx_ocall(0, ms);

	if (status == SGX_SUCCESS) {
	}
	sgx_ocfree();
	return status;
}

