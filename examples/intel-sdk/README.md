# Intel SDK example

To test the new ABI layer with the Intel SDK, the following steps need to be followed:

1. Build the ABI library (see `../../README.md`)
2. Patch the Intel SDK
3. Generate a test enclave (see `test-enclave/README.md`)

## Patching the Intel SDK

First make sure you have pulled the submodules included in this repository by using

```bash
git submodule update --init --recursive
```

In the `linux-sdk` repository, you can apply the patches present in this directory with the command

```bash
git apply ../patches/*.patch
```

Finally you should rebuild the SDK and execute the installer as explained in the `linux-sdk/README.MD`.

