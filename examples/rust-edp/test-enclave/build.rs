use std::env;

fn main() {
    std::env::set_var("RUSTFLAGS", "-C link-dead-code");
    let manifest_dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    println!("cargo:rustc-link-search={}/../build/", manifest_dir);
    println!("cargo:rustc-link-lib=static=unified-asl");
}
