# Rust EDP example

To test the new ABI layer with the Rust EDP, the following steps need to be followed:

1. Configure and build the ABI library
2. Update submodules
2. Patch & build the Rust compiler
3. Setup rustup toolchain
3. Patch & build the Fortanix tools
4. Run the test enclave

## Configure and build the ABI library

The configuration can be done in the `rust-edp-congig.h` file.
Building can be done through the Makefile in this directory.

```bash
make
```

For a debug build:

```bash
make DEBUG=1
```

## Update submodules

```bash
git submodule update --init --recursive
```

This will clone two repositories:
* `rust` → the Rust compiler
* `rust-sgx` → The Fortanix tools

## Patch & build the Rust compiler

In the `rust` repository, first apply the patch:

```bash
git apply ../patches/rust.patch
```

Next copy the provided configuration file to the repository:

```bash 
cp ../rust-config.toml config.toml
```

Make sure all the dependencies listed in `rust/README.md` are satisfied and build the compiler with the following command:

```bash
./x.py build --stage 1 -i --target x86_64-fortanix-unknown-sgx
```

This will only build the first stage, but this is sufficient for our changes.

## Setup rustup toolchain

Install rustup and execute the following commands:

```bash
rustup toolchain link unified-asl toolchain/
cd test-enclave && rustup override set unified-asl
```

## Patch & build the Fortanix tools

In the `rust-sgx` repository, first apply the patch:

```bash
git apply ../patches/rust-sgx.patch
```

And install the tools with:

```bash
cargo install --path fortanix-sgx-tools/
cargo install --path sgxs-tools/
```

## Run the test enclave

This is simply done by executing `cargo run` in the `test-enclave` directory.
