#define TLS_TRUSTED_SP              0x10    // Offset in the thread local storage (TLS) where the last 
                                            // trusted stack pointer is stored.

#define ECALL_STACK_OFFSET          -4096   // Location of the ECALL stack base, defined as an 
                                            // offset to the TCS.

#define API_ROOT_ECALL_FUNC         root_entry
#define API_NESTED_ECALL_FUNC       nested_entry

#define SIMULATION_MODE             0

#define EXCEPTION_RESOLVEMENT       0

