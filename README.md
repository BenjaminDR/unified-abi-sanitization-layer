# Unified ABI Sanitization Layer

## Building

You can build the static library that includes the ABI Sanitization Layer by using

```bash
make
```

If you want to include debug information, you can use

```bash
make DEBUG=1
```

## Thread Local Storage

This ABI layer makes use of the TLS to store the address of the trusted stack.
In the configuration header file of the stub an offset in the TLS can be specified that can be used with the definition

```c

#define TLS_TRUSTED_SP              8       // Offset in the thread local storage (TLS) where the last 
                                            // trusted stack pointer is stored.
```

**Important:** The ABI expects this location to be initialized to 0 when building the enclave.
