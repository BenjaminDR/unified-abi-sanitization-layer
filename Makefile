DEBUG ?= 1

CC := gcc
CCFLAGS := -Wall -Wno-int-conversion
DBGFLAGS := -ggdb -O0
ifeq ($(DEBUG), 1)
	CCFLAGS += $(DBGFLAGS)
endif

TARGET := libunified-asl.a

ASM_SRC := src/asl-stub.S

OBJS := $(ASM_SRC:.S=.o)

default: all

$(TARGET): $(OBJS)
	ar rsD $@ $(OBJS)


src/%.o: src/%.S
	$(CC) $(CCFLAGS) -c -o $@ $<

src/%.o: src/%.c
	$(CC) $(CCFLAGS) -c -o $@ $<

.PHONY: all
all: $(TARGET)

.PHONY: clean
clean: 
	rm -f $(TARGET) $(OBJS) src/asl-stub.s


.PHONY: preprocess
preprocess: src/asl-stub.s

src/asl-stub.s: src/asl-stub.S
	$(CC) -E -o $@ $<

